public abstract with sharing class ControllerAdapter {

	public static ControllerAdapter adapt(ApexPages.StandardController controller) {
		return new StandardControllerAdapter(controller);
	}

	public static ControllerAdapter adapt (ApexPages.StandardSetController controller) {
		return new StandardSetControllerAdapter(controller);
	}

	public abstract List<SObject> getSelected();

	public class StandardControllerAdapter extends ControllerAdapter {
		ApexPages.StandardController controller;

		public StandardControllerAdapter(ApexPages.StandardController controller) {
			this.controller = controller;
		}

		public override List<SObject> getSelected() {
			List<SObject> result = new List<SObject>();
			result.add(controller.getRecord());
			return result;
		}
	}

	public class StandardSetControllerAdapter extends ControllerAdapter {
		ApexPages.StandardSetController controller;

		public StandardSetControllerAdapter(ApexPages.StandardSetController controller) {
			this.controller = controller;
		}

		public override List<SObject> getSelected() {
			return controller.getSelected();
		}
	}

}